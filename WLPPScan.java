import java.util.*;
import java.math.*;

public class WLPPScan {
    public static final void main(String[] args) {
        new WLPPScan().run();
    }
    
    private Lexer lexer = new Lexer();

    private void run() {
        Scanner in = new Scanner(System.in);
        
        while(in.hasNextLine()) {
            String line = in.nextLine();

            // Scan the line into an array of tokens
            Token[] tokens;
            tokens = lexer.scan(line);
            
            for(int i = 0; i < tokens.length; i++){
                String myID = tokens[i].lexeme;
                if(tokens[i].kind == Kind.ID){
                    //System.out.println("it IS ID " + tokens[i].lexeme.toString());
                    if(myID.equals("return")) System.out.println("RETURN " + tokens[i].lexeme);
                    else if(myID.equals("if")) System.out.println("IF " + tokens[i].lexeme);
                    else if(myID.equals("else")) System.out.println("ELSE " + tokens[i].lexeme);
                    else if(myID.equals("while")) System.out.println("WHILE " + tokens[i].lexeme);
                    else if(myID.equals("println")) System.out.println("PRINTLN " + tokens[i].lexeme);
                    else if(myID.equals("wain")) System.out.println("WAIN " + tokens[i].lexeme);
                    else if(myID.equals("new")) System.out.println("NEW " + tokens[i].lexeme);
                    else if(myID.equals("delete")) System.out.println("DELETE " + tokens[i].lexeme);
                    else if(myID.equals("int")) System.out.println("INT " + tokens[i].lexeme);
                    else if(myID.equals("NULL")) System.out.println("NULL " + tokens[i].lexeme);
                    else System.out.println("ID " + tokens[i].lexeme);
                }//if
                else if(tokens[i].kind == Kind.NUM || tokens[i].kind == Kind.ZERO) System.out.println("NUM " + tokens[i].lexeme);
                else if(tokens[i].kind == Kind.LPAREN) System.out.println("LPAREN " + tokens[i].lexeme);
                else if(tokens[i].kind == Kind.RPAREN) System.out.println("RPAREN " + tokens[i].lexeme);
                else if(tokens[i].kind == Kind.LBRACE) System.out.println("LBRACE " + tokens[i].lexeme);
                else if(tokens[i].kind == Kind.RBRACE) System.out.println("RBRACE " + tokens[i].lexeme);
                else if(tokens[i].kind == Kind.BECOMES) System.out.println("BECOMES " + tokens[i].lexeme);
                else if(tokens[i].kind == Kind.EQ) System.out.println("EQ " + tokens[i].lexeme);
                else if(tokens[i].kind == Kind.NE) System.out.println("NE " + tokens[i].lexeme);
                else if(tokens[i].kind == Kind.LT) System.out.println("LT " + tokens[i].lexeme);
                else if(tokens[i].kind == Kind.LE) System.out.println("LE " + tokens[i].lexeme);
                else if(tokens[i].kind == Kind.GT) System.out.println("GT " + tokens[i].lexeme);
                else if(tokens[i].kind == Kind.GE) System.out.println("GE " + tokens[i].lexeme);
                else if(tokens[i].kind == Kind.PLUS) System.out.println("PLUS " + tokens[i].lexeme);
                else if(tokens[i].kind == Kind.MINUS) System.out.println("MINUS " + tokens[i].lexeme);
                else if(tokens[i].kind == Kind.STAR) System.out.println("STAR " + tokens[i].lexeme);
                else if(tokens[i].kind == Kind.SLASH) System.out.println("SLASH " + tokens[i].lexeme);
                else if(tokens[i].kind == Kind.PCT) System.out.println("PCT " + tokens[i].lexeme);
                else if(tokens[i].kind == Kind.COMMA) System.out.println("COMMA " + tokens[i].lexeme);
                else if(tokens[i].kind == Kind.SEMI) System.out.println("SEMI " + tokens[i].lexeme);
                else if(tokens[i].kind == Kind.LBRACK) System.out.println("LBRACK " + tokens[i].lexeme);
                else if(tokens[i].kind == Kind.RBRACK) System.out.println("RBRACK " + tokens[i].lexeme);
                else if(tokens[i].kind == Kind.AMP) System.out.println("AMP " + tokens[i].lexeme);
                else if(tokens[i].kind == Kind.COMMENT) break;
            }//for
        }//while
    }//run
}//WLPPScan
        

/** The various kinds of tokens. */
enum Kind {
    COMMENT,
    ID,
    NUM,
    ZERO,
    LPAREN,
    RPAREN,
    LBRACE,
    RBRACE,
    BECOMES,
    EQ,
    NE,
    LT,
    GT,
    LE,
    GE,
    PLUS,
    MINUS,
    STAR,
    SLASH,
    PCT,
    COMMA,
    SEMI,
    LBRACK,
    RBRACK,
    AMP,
    WHITESPACE,
}

/** Representation of a token. */
class Token {
    public Kind kind;     // The kind of token.
    public String lexeme; // String representation of the actual token in the
                          // source code.

    public Token(Kind kind, String lexeme) {
        this.kind = kind;
        this.lexeme = lexeme;
    }
    public String toString() {
        return kind+" {"+lexeme+"}";
    }
    private int parseLiteral(String s, int base, int bits) {
        BigInteger x = new BigInteger(s, base);
        if(x.signum() > 0) {
            if(x.bitLength() > bits) {
                System.err.println("ERROR in parsing: constant out of range: "+s);
                System.exit(1);
            }
        } else if(x.signum() < 0) {
            if(x.negate().bitLength() > bits-1
            && x.negate().subtract(new BigInteger("1")).bitLength() > bits-1) {
                System.err.println("ERROR in parsing: constant out of range: "+s);
                System.exit(1);
            }
        }
        return (int) (x.longValue() & ((1L << bits) - 1));
    }
}

/** Lexer -- reads an input line, and partitions it into a list of tokens. */
class Lexer {
    public Lexer() {
        CharSet whitespace = new Chars("\t\n\r ");
        CharSet letters = new Chars(
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz");
        CharSet lettersDigits = new Chars(
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");
        CharSet digits = new Chars("0123456789");
        CharSet oneToNine = new Chars("123456789");
        CharSet all = new AllChars(); 

        table = new Transition[] {
                new Transition(State.START, whitespace, State.WHITESPACE),
                new Transition(State.START, letters, State.ID),
                new Transition(State.ID, lettersDigits, State.ID),
                new Transition(State.START, new Chars("0"), State.ZERO),
                new Transition(State.START, oneToNine, State.NUM),
                new Transition(State.NUM, digits, State.NUM),
                new Transition(State.START, new Chars("("), State.LPAREN),
                new Transition(State.START, new Chars(")"), State.RPAREN),
                new Transition(State.START, new Chars("{"), State.LBRACE),
                new Transition(State.START, new Chars("}"), State.RBRACE),
                new Transition(State.START, new Chars("="), State.BECOMES),
                new Transition(State.BECOMES, new Chars("="), State.EQ),
                new Transition(State.START, new Chars("!"), State.EXCLAMATION),
                new Transition(State.EXCLAMATION, new Chars("="), State.NE),
                new Transition(State.START, new Chars(">"), State.GT),
                new Transition(State.GT, new Chars("="), State.GE),
                new Transition(State.START, new Chars("<"), State.LT),
                new Transition(State.LT, new Chars("="), State.LE),
                new Transition(State.START, new Chars("+"), State.PLUS),
                new Transition(State.START, new Chars("-"), State.MINUS),
                new Transition(State.START, new Chars("*"), State.STAR),
                new Transition(State.START, new Chars("/"), State.SLASH),
                new Transition(State.SLASH, new Chars("/"), State.COMMENT),
                new Transition(State.START, new Chars("%"), State.PCT),
                new Transition(State.START, new Chars(","), State.COMMA),
                new Transition(State.START, new Chars(";"), State.SEMI),
                new Transition(State.START, new Chars("["), State.LBRACK),
                new Transition(State.START, new Chars("]"), State.RBRACK),
                new Transition(State.START, new Chars("&"), State.AMP),
        };
    }
    /** Partitions the line passed in as input into an array of tokens.
     * The array of tokens is returned.
     */
    public Token[] scan( String input ) {
        List<Token> ret = new ArrayList<Token>();
        if(input.length() == 0) return new Token[0];
        int i = 0;
        int startIndex = 0;
        State state = State.START;
        while(true) {
            Transition t = null;
            if(i < input.length()) t = findTransition(state, input.charAt(i));
            if(t == null) {
                // no more transitions possible
                if(!state.isFinal()) {
                    System.err.println("ERROR in lexing after reading "+input.substring(0, i));
                    System.exit(1);
                }
                if( state.kind != Kind.WHITESPACE ) {
                    ret.add(new Token(state.kind,
                                input.substring(startIndex, i)));
                }
                startIndex = i;
                state = State.START;
                if(i >= input.length()) break;
            } else {
                state = t.toState;
                i++;
            }
        }
        return ret.toArray(new Token[ret.size()]);
    }

    ///////////////////////////////////////////////////////////////
    // END OF PUBLIC METHODS
    ///////////////////////////////////////////////////////////////

    private Transition findTransition(State state, char c) {
        for( int j = 0; j < table.length; j++ ) {
            Transition t = table[j];
            if(t.fromState == state && t.chars.contains(c)) {
                return t;
            }
        }
        return null;
    }

    private static enum State {
        START(null),
        EXCLAMATION(null),
        COMMENT(Kind.COMMENT),
        ID(Kind.ID),
        NUM(Kind.NUM),
        ZERO(Kind.ZERO),
        LPAREN(Kind.LPAREN),
        RPAREN(Kind.RPAREN),
        LBRACE(Kind.LBRACE),
        RBRACE(Kind.RBRACE),
        BECOMES(Kind.BECOMES),
        EQ(Kind.EQ),
        NE(Kind.NE),
        LT(Kind.LT),
        GT(Kind.GT),
        LE(Kind.LE),
        GE(Kind.GE),
        PLUS(Kind.PLUS),
        MINUS(Kind.MINUS),
        STAR(Kind.STAR),
        SLASH(Kind.SLASH),
        PCT(Kind.PCT),
        COMMA(Kind.COMMA),
        SEMI(Kind.SEMI),
        LBRACK(Kind.LBRACK),
        RBRACK(Kind.RBRACK),
        AMP(Kind.AMP),
        WHITESPACE(Kind.WHITESPACE);
        State(Kind kind) {
            this.kind = kind;
        }
        Kind kind;
        boolean isFinal() {
            return kind != null;
        }
    }

    private interface CharSet {
        public boolean contains(char newC);
    }
    private class Chars implements CharSet {
        private String chars;
        public Chars(String chars) { this.chars = chars; }
        public boolean contains(char newC) {
            return chars.indexOf(newC) >= 0;
        }
    }
    private class AllChars implements CharSet {
        public boolean contains(char newC) {
            return true;
        }
    }

    private class Transition {
        State fromState;
        CharSet chars;
        State toState;
        Transition(State fromState, CharSet chars, State toState) {
            this.fromState = fromState;
            this.chars = chars;
            this.toState = toState;
        }
    }
    private Transition[] table;
}

