import java.util.*;

public class WLPPGen {
    Scanner in = new Scanner(System.in);
    public HashMap<String, String> symbol_table = new HashMap<String, String>();
    public HashMap<String, Integer> offset_table = new HashMap<String, Integer>();
    List<String> symbols;
    
    // The set of terminal symbols in the WLPP grammar.
    Set<String> terminals = new HashSet<String>(Arrays.asList("BOF", "BECOMES", 
         "COMMA", "ELSE", "EOF", "EQ", "GE", "GT", "ID", "IF", "INT", "LBRACE", 
         "LE", "LPAREN", "LT", "MINUS", "NE", "NUM", "PCT", "PLUS", "PRINTLN",
         "RBRACE", "RETURN", "RPAREN", "SEMI", "SLASH", "STAR", "WAIN", "WHILE",
         "AMP", "LBRACK", "RBRACK", "NEW", "DELETE", "NULL"));
         
    // Data structure for storing the parse tree.
    public class Tree {
        List<String> rule;
        ArrayList<Tree> children = new ArrayList<Tree>();
        // Does this node's rule match otherRule?
        boolean matches(String otherRule) {
            return tokenize(otherRule).equals(rule);
        }
    }

    // Divide a string into a list of tokens.
    List<String> tokenize(String line) {
        List<String> ret = new ArrayList<String>();
        Scanner sc = new Scanner(line);
        while (sc.hasNext()) {
            ret.add(sc.next());
        }
        return ret;
    }

    // Read and return wlppi parse tree
    Tree readParse(String lhs) {
        if ( !in.hasNextLine() ) {
           System.err.println( "something is wrong here" );
           System.exit(0);
        }
        String line = in.nextLine();
        List<String> tokens = tokenize(line);
        Tree ret = new Tree();
        ret.rule = tokens;
        if (!terminals.contains(lhs)) {
            Scanner sc = new Scanner(line);
            sc.next(); // discard lhs
            while (sc.hasNext()) {
                String s = sc.next();
                ret.children.add(readParse(s));
            }
        }
        return ret;
    }

    // Compute symbols defined in t
    List<String> genSymbols(Tree t) {
        return null;
    }
 
    // Print an error message and exit the program.
    void bail(String msg) {
        System.err.println("ERROR: " + msg);
        System.exit(0);
    }

    // Generate the code for the parse tree t.
    String genCode(Tree node) {
        return null;
    }
    
    int offset = 4;
    
    //Prologue code
    public void prologueCode() {
        String ret = ".import print\n"
        +".import init\n"
        +".import new\n"
        +".import delete\n" //imports
        +"lis $4\n"
        +".word 4\n" //$4 is 4
        +"lis $10\n"
        +".word print\n" //$10 is print
        +"lis $11\n"
        +".word 1\n" //$11 is 1
        +"lis $12\n"
        +".word new\n" //$12 is new
        +"lis $13\n"
        +".word delete\n" //$13 is delete
        +"lis $14\n"
        +".word init\n" //$14 is init
        +"sub $29, $30, $4\n" //$29 points to bottom of stack
        +"sw $31, -4($30)\n"
        +"sub $30, $30, $4\n" //store $31
        +"sw $1, -4($30)\n"
        +"sub $30, $30, $4\n" //store $1
        +"sw $2, -4($30)\n"
        +"sub $30, $30, $4\n"; //store $2
        System.out.println(ret);
    }
    
    void push(String reg) {
        System.out.println("sw "+reg+", -4($30)\n" + "sub $30, $30, $4");
    }
    
    void pop(String reg) {
        System.out.println("lw "+reg+", 0($30)\n" + "add $30, $30, $4");
    }
    
    int iterator = 0;
    // Generate code for expr
    void exprCode(Tree node) {
        int Y = iterator;
        String ret = "";
        if( node.matches("dcls dcls dcl BECOMES NUM SEMI") ) {
            System.err.println("declaration!!!");
            System.err.println(node.children.get(1).children.get(1).rule.get(1));
            exprCode(node.children.get(0));
            ret = "lis $19\n"
            + ".word "+node.children.get(3).rule.get(1);
            System.out.println(ret);
            push("$19");
        }
        else if( node.matches("dcls dcls dcl BECOMES NULL SEMI") ) {
            System.err.println("dcl NULL");
            System.err.println(node.children.get(1).children.get(1).rule.get(1));
            exprCode(node.children.get(0));
            push("$0");
        }
        else if( node.matches("statement IF LPAREN test RPAREN LBRACE statements RBRACE ELSE LBRACE statements RBRACE") ) {
            iterator++;
            exprCode( node.children.get(2) );
            System.out.println("beq $3, $0, else"+Y);
            exprCode( node.children.get(5) );
            System.out.println("beq $0, $0, endif"+Y);
            System.out.println("else"+Y+": ");
            exprCode( node.children.get(9) );
            System.out.println("endif"+Y+": ");
        }
        else if( node.matches("statement WHILE LPAREN test RPAREN LBRACE statements RBRACE") ) {
            iterator++;
            System.out.println("loop"+Y+": ");
            exprCode( node.children.get(2) );
            System.out.println("beq $3, $0, done"+Y);
            exprCode( node.children.get(5) );
            System.out.println("beq $0, $0, loop"+Y);
            System.out.println("done"+Y+": ");
        }
        else if( node.matches("statement expr BECOMES expr SEMI") ) {
            boolean typeisINTSTAR = testDereference(node.children.get(0));
            exprCode(node.children.get(2));
            if( !typeisINTSTAR ) {
                String name = retName(node.children.get(0));
                if( offset_table.containsKey(name) ) {
                    int offset = -1*offset_table.get(name);
                    System.out.println("sw $3, "+offset+"($29)");
                }// THIS IF HAS TO BE TRUE!!!
            }
            else if( typeisINTSTAR ) {
                push("$3");
                exprCode(retDereference(node.children.get(0)));
                pop("$5");
                System.out.println("sw $5, 0($3)");
            }
        }
        else if( node.matches("statement PRINTLN LPAREN expr RPAREN SEMI") ) {
            System.err.println("print hath been called upon!");
            exprCode( node.children.get(2) );
            push("$1");
            System.out.println("add $1, $3, $0\n" + "jalr $10"); //call print
            pop("$1");
        }
        else if( node.matches("statement DELETE LBRACK RBRACK expr SEMI") ) {
            exprCode(node.children.get(3));
            System.out.println("add $1, $3, $0\n" + "jalr $13"); //call delete
        }
        else if( node.matches("test expr LT expr") || 
                 node.matches("test expr GT expr") ||
                 node.matches("test expr NE expr") || 
                 node.matches("test expr EQ expr") ||
                 node.matches("test expr LE expr") ||
                 node.matches("test expr GE expr") ) {
            exprCode( node.children.get(0) );
            push("$3");
            exprCode( node.children.get(2) );
            pop("$5");
            String type = retType(node.children.get(0));
            if( type == "int") {
                System.err.println("int comparison!");
                if( node.matches("test expr LT expr") ) {
                    System.out.println("slt $3, $5, $3");
                }
                else if( node.matches("test expr GT expr") ) {
                    System.out.println("slt $3, $3, $5");
                }
                else if( node.matches("test expr NE expr")  || node.matches("test expr EQ expr") ) {
                    System.out.println("slt $6, $5, $3");
                    System.out.println("slt $7, $3, $5");
                    System.out.println("add $3, $6, $7");
                    if( node.matches("test expr EQ expr") ) {
                         System.out.println("sub $3, $11, $3");
                    }
                }
                else if( node.matches("test expr LE expr") || node.matches("test expr GE expr") ) { 
                   System.out.println("slt $6, $5, $3");//LT in $6
                   System.out.println("slt $7, $3, $5");//GT in $7
                   System.out.println("add $9, $6, $7");
                   System.out.println("sub $9, $11, $9");//EQ in $9
                   if( node.matches("test expr LE expr") ) {
                       System.out.println("add $3, $9, $6");//LE in $3
                   }
                   else if( node.matches("test expr GE expr") ) {
                       System.out.println("add $3, $9, $7");//GE in $3
                   }
               }
           }
           else if( type == "int*") {
                System.err.println("int* comparison!");
                if( node.matches("test expr LT expr") ) {
                    System.out.println("sltu $3, $5, $3");
                }
                else if( node.matches("test expr GT expr") ) {
                    System.out.println("sltu $3, $3, $5");
                }
                else if( node.matches("test expr NE expr")  || node.matches("test expr EQ expr") ) {
                    System.out.println("sltu $6, $5, $3");
                    System.out.println("sltu $7, $3, $5");
                    System.out.println("add $3, $6, $7");
                    if( node.matches("test expr EQ expr") ) {
                         System.out.println("sub $3, $11, $3");
                    }
                }
                else if( node.matches("test expr LE expr") || node.matches("test expr GE expr") ) { 
                   System.out.println("sltu $6, $5, $3");//LT in $6
                   System.out.println("sltu $7, $3, $5");//GT in $7
                   System.out.println("add $9, $6, $7");
                   System.out.println("sub $9, $11, $9");//EQ in $9
                   if( node.matches("test expr LE expr") ) {
                       System.out.println("add $3, $9, $6");//LE in $3
                   }
                   else if( node.matches("test expr GE expr") ) {
                       System.out.println("add $3, $9, $7");//GE in $3
                   }
               }
           }
        }
        else if( node.matches("expr expr PLUS term") || 
                 node.matches("expr expr MINUS term") ||
                 node.matches("term term STAR factor") ||
                 node.matches("term term SLASH factor") ||
                 node.matches("term term PCT factor") ) {
            String type1 = retType(node.children.get(0));
            String type2 = retType(node.children.get(2));
            if( type1 == "int" && type2 == "int" ) {     
                exprCode( node.children.get(0) );
                push("$3");
                exprCode( node.children.get(2) );
                pop("$5");
                if( node.matches("expr expr PLUS term") ) {
                    System.out.println("add $3, $5, $3");
                }
                else if( node.matches("expr expr MINUS term") ) {
                    System.out.println("sub $3, $5, $3");
                }
                else if( node.matches("term term STAR factor") ) {
                    System.out.println("mult $5, $3\n" + "mflo $3");
                }
                else if( node.matches("term term SLASH factor") ) {
                    System.out.println("div $5, $3\n" + "mflo $3");
                }
                else if( node.matches("term term PCT factor") ) {
                    System.out.println("div $5, $3\n" + "mfhi $3");
                }
            }
            if( type1 == "int*" && type2 == "int" ) {
                exprCode( node.children.get(0) );
                push("$3");
                exprCode( node.children.get(2) );
                System.out.println("mult $3, $4\n" + "mflo $3");
                pop("$5");
                if( node.matches("expr expr PLUS term") ) {
                    System.out.println("add $3, $5, $3");
                }
                else if( node.matches("expr expr MINUS term") ) {
                    System.out.println("sub $3, $5, $3");
                }
            }
            if( type1 == "int" && type2 == "int*" ) {
                if( node.matches("expr expr PLUS term") ) {
                    exprCode( node.children.get(0) );
                    System.out.println("mult $3, $4\n" + "mflo $3");
                    push("$3");
                    exprCode( node.children.get(2) );
                    pop("$5");
                    System.out.println("add $3, $5, $3");
                }
            }
            if( type1 == "int*" && type2 == "int*" ) {
                if( node.matches("expr expr MINUS term") ) {
                exprCode( node.children.get(0) );
                push("$3");
                exprCode( node.children.get(2) );
                pop("$5");
                System.out.println("sub $3, $5, $3\n" + "div $3, $4\n" + "mflo $3");
                }
            }
        }
        else if( node.matches("expr term") ) {
            exprCode(node.children.get(0));
        }
        else if( node.matches("term factor") ) {
            exprCode(node.children.get(0));
        }
        else if( node.matches("factor NUM") ) {
            System.out.println("lis $3\n" + ".word "+node.children.get(0).rule.get(1));
        }
        else if( node.matches("factor NULL") ) {
            System.out.println("add $3, $0, $0");
        }
        else if( node.matches("factor ID") ) {
            String name = node.children.get(0).rule.get(1);
            if( offset_table.containsKey(name) ) {
                int offset = -1*offset_table.get(name);
                System.out.println("lw $3, "+offset+"($29)");
            }// THIS IF HAS TO BE TRUE!!!
        }
        else if( node.matches("factor STAR factor") ) {
            exprCode(node.children.get(1));
            System.out.println("lw $3, 0($3)");
        }
        else if( node.matches("factor AMP factor") ) {
            String type = retType( node.children.get(1) );
            if( node.children.get(1).matches("factor ID") ) {
                String name = node.children.get(1).children.get(0).rule.get(1);
                if( offset_table.containsKey(name) ) {
                    int offset = -1*offset_table.get(name);
                    System.out.println("lis $3\n" + ".word "+offset+"\n" + "add $3, $29, $3");
                }// THIS IF HAS TO BE TRUE!!!
            }
            else if( node.children.get(1).matches("factor STAR factor") ) {
                exprCode( node.children.get(1).children.get(1) );
            }
        }
        else if( node.matches("factor NEW INT LBRACK expr RBRACK") ) {
            exprCode(node.children.get(3));
            System.out.println("add $1, $3, $0\n" + "jalr $12"); //call new
        }
        else if( node.matches("procedure INT WAIN LPAREN dcl COMMA dcl RPAREN LBRACE dcls statements RETURN expr SEMI RBRACE") ) {
            System.err.println("reached procedure -> shitz");
            String type = retType(node.children.get(3));
            if( type == "int" ){
                System.err.println("a is an int -> setting $2 to 0!");
                System.out.println("add $2, $0, $0");
            }
            else {
                System.err.println("a is an int* -> not messing with $2!");
            }
            System.err.println("calling init nao!");
            System.out.println("jalr $14"); //call init
            System.err.println("called init!");
            if( !node.children.isEmpty() ) {
                for( int i = 0; i < node.children.size(); i++ ){
                     exprCode( node.children.get(i) );
                }// for
            }// if non-terminal node
        }
        else if( !node.children.isEmpty() ) {
            for( int i = 0; i < node.children.size(); i++ ){
                exprCode( node.children.get(i) );
            }// for
        }// if non-terminal node
    }
    
    String retName( Tree node ) {
        if( node.matches("factor ID") ) {
            return node.children.get(0).rule.get(1);
        }
        else if( !node.children.isEmpty() ) {
            for( int i = 0; i < node.children.size(); i++ ){
                return retName( node.children.get(i) );
            }
        }
        return "shouldnevergethere";
    }
    
    boolean testDereference( Tree node ) {
        if( node.matches("factor STAR factor") ) {
            return true;
        }
        else if( !node.children.isEmpty() ) {
            for( int i = 0; i < node.children.size(); i++ ){
                return testDereference( node.children.get(i) );
            }
        }
        return false;
    }
    
    Tree retDereference( Tree node ) {
        if( node.matches("factor STAR factor") ) {
            System.err.println("now returning factor from star factor");
            return node.children.get(1);
        }
        else if( !node.children.isEmpty() ) {
            for( int i = 0; i < node.children.size(); i++ ){
                return retDereference( node.children.get(i) );
            }
        }
        return node;//this should never be returned (this function is only called if testdereference is true!!!
    }
    
    // Epilogue code
    public void epilogueCode() {
        System.out.println("");
        System.out.println("add $30, $29, $0");
        pop("$31");
        System.out.println("jr $31");
    }
    
    // Build a symbol table
    public HashMap<String, String> buildTable( Tree node, HashMap<String, String> symboltable, HashMap<String, Integer> offsettable ){
        //int offset = 0;
        if( node.matches("dcl type ID") ) {
            String name = node.children.get(1).rule.get(1);
            String type = "";
            if( symboltable.containsKey(name) ) {
                System.err.println( "ERROR: duplicate declarations of " + name );
                System.exit(0);
            }// if name already in table
            if( node.children.get(0).matches("type INT") ){
                type = "int";
            } // if type INT
            else {
                type = "int*";
            }// else
            symboltable.put(name, type);
            offsettable.put(name, offset);
            offset += 4;
        }// if dcl type ID
        if( !node.children.isEmpty() ) {
            for( int i = 0; i < node.children.size(); i++ ){
                symboltable = buildTable( node.children.get(i), symboltable, offsettable );
            }// for
        }// if non-terminal node
        return symboltable;
    }// buildTable
    
    
    // prints the symbol table
    public void print_symbol_table(){
        // for each key in keyset of symbol_table
        for (String key : symbol_table.keySet()){ 
            System.err.println( key + " " + symbol_table.get(key));
        }// for
    }// print_symbol_table
    
    
    // prints the offset table
    public void print_offset_table(){
        // for each key in keyset of symbol_table
        for (String key : offset_table.keySet()){ 
            String offset = offset_table.get(key).toString();
            System.err.println( key + " " + offset);
        }// for
    }// print_symbol_table
    
    
    // Check uses of variables
    public void checkUses( Tree node, HashMap<String, String> table ){
        if( node.matches("factor ID") ) {
            String name = node.children.get(0).rule.get(1);
            if( !table.containsKey(name) ) {
                System.err.println( "ERROR: use of undeclared variable " + name );
                System.exit(0);
            }// if name not in table
        }// if factor ID
        if( !node.children.isEmpty() ) {
            for( int i = 0; i < node.children.size(); i++ ){
                checkUses( node.children.get(i), table );
            }// for
        }// if non-terminal node
    }// checkUses
    
    
    // checks if node is an l-value
    public boolean isLValue( Tree node ) {
        if( node.matches("factor ID") || node.matches("factor STAR factor") ) {
            return true;
        } else if( node.matches("expr term") || node.matches("term factor") ) {
            return isLValue( node.children.get(0) );
        } else if ( node.matches("factor LPAREN expr RPAREN") ) {
            return isLValue( node.children.get(1) );
        } else {
            return false;
        }// endif
    }// isLValue
    
    
    // checking l-values
    public void checkLValues( Tree node ) {
        if( node.matches("statement expr BECOMES expr SEMI") ) {
            if( !isLValue(node.children.get(0) ) ) {
                bail("lhs of assignment is not an lvalue!");
            }
        } else if( node.matches("factor AMP factor") ) {
            if( !isLValue(node.children.get(1) ) ) {
                bail("can't take address of a non-lvalue!");
            }
        }// if
        if( !node.children.isEmpty() ) {
            for( int i = 0; i < node.children.size(); i++ ){
                checkLValues( node.children.get(i) );
            }// for
        }// if non-terminal node
    }// checkLValues
    
    
    // determine if type is valid and if so return it
    public String detValidType( Tree node ) {
        String type1;
        String type2;
        if( node.matches("procedure INT WAIN LPAREN dcl COMMA dcl RPAREN LBRACE dcls statements RETURN expr SEMI RBRACE") ) {
            type1 = retType( node.children.get(5) );
            type2 = detValidType( node.children.get(11) );
            if( type1 == "int" && type2 == "int") {
                if( !node.children.isEmpty() ) {
                    for( int i = 0; i < node.children.size(); i++ ){
                         detValidType( node.children.get(i) );
                     }// for
                 }// if non-terminal node
            } else {
                bail("second input to wain can't be a pointer/ can't return a pointer!");
            }
        }
        else if( node.matches("dcls dcls dcl BECOMES NUM SEMI") ) {
            type1 = retType( node.children.get(1) );
            if( type1 == "int" ) {
                if( !node.children.isEmpty() ) {
                    for( int i = 0; i < node.children.size(); i++ ){
                         detValidType( node.children.get(i) );
                     }// for
                 }// if non-terminal node
            } else {
                bail("can't declare a pointer to be a number!");
            }
        }
        else if( node.matches("dcls dcls dcl BECOMES NULL SEMI") ) {
            type1 = retType( node.children.get(1) );
            if( type1 == "int*" ) {
                if( !node.children.isEmpty() ) {
                    for( int i = 0; i < node.children.size(); i++ ){
                         detValidType( node.children.get(i) );
                     }// for
                 }// if non-terminal node
            } else {
                bail("can't declare an int to be NULL!");
            }
        }
        else if( node.matches("statement PRINTLN LPAREN expr RPAREN SEMI") ) {
            type1 = detValidType( node.children.get(2) );
            if (type1 == "int") {
                return "int";
            } else {
                bail("can't print an int*!");
            }
        }
        else if( node.matches("statement DELETE LBRACK RBRACK expr SEMI") ) {
            type1 = detValidType( node.children.get(3) );
            if (type1 == "int*") {
                return "int*";
            } else {
                bail("can't delete an int!");
            }
        }
        else if( node.matches("test expr EQ expr") ||
                 node.matches("test expr NE expr") ||
                 node.matches("test expr LT expr") ||
                 node.matches("test expr LE expr") ||
                 node.matches("test expr GE expr") ||
                 node.matches("test expr GT expr") ||
                 node.matches("statement expr BECOMES expr SEMI") ) {
            type1 = detValidType( node.children.get(0) );
            type2 = detValidType( node.children.get(2) );
            if( type1 == type2 ){
                return type1;
            } else {
                bail("can't perform comparisons/assignments with a pointer and an int!");
            }
        }
        else if( node.matches("expr term") ) {
            type1 = retType( node.children.get(0) );
            return type1;
        }
        else if( node.matches("expr expr PLUS term") ){
            type1 = retType( node.children.get(0) );
            type2 = retType( node.children.get(2) );
            if( type1 == "int*" && type2 == "int*" ) {
                bail("can't add two pointers!");
            }//if
            else {
                if( type1 == "int*" || type2 == "int*" ) {
                    return "int*";
                } else {
                    return "int";
                    }
            }
        }// expr plus term
        else if( node.matches("expr expr MINUS term") ) {
            type1 = retType( node.children.get(0) );
            type2 = retType( node.children.get(2) );
            if( type1 == "int" && type2 == "int*" ) {
                bail("can't subtract a pointer from an integer!");
            }//if
            else {
                if( type1 == "int*" && type2 == "int" ) {
                    return "int*";
                } else if( type1 == "NULL" ) {
                    bail("can't subtract from a null pointer!");
                } else {
                    return "int";
                    }
            }
        }//expr minus term
        else if( !node.children.isEmpty() ) {
            for( int i = 0; i < node.children.size(); i++ ){
                detValidType( node.children.get(i) );
            }// for
        }// if non-terminal node
        return "boo";
    }
       
    // helper to return type of expr
    public String retType( Tree node ) {
        if( node.matches("dcl type ID") ) {
            return symbol_table.get( node.children.get(1).rule.get(1) );
        }
        else if( node.matches("term factor") ) {
            return retType( node.children.get(0) );
        }
        else if( node.matches("term term STAR factor") || node.matches("term term SLASH factor") || node.matches("term term PCT factor") ) {
            if( retType(node.children.get(0)) == "int" && retType(node.children.get(2)) == "int" ) {
                return "int";
            }
            else {
            bail("can't perform this operation on pointers!");
            }
        }
        else if( node.matches("factor ID") ) {
            return symbol_table.get( node.children.get(0).rule.get(1) );
        }
        else if( node.matches("factor NUM") ) {
            return "int";
        }
        else if( node.matches("factor NULL") ) {
            return "int*";
        }
        else if( node.matches("factor LPAREN expr RPAREN") ) {
            return detValidType(node.children.get(1));
        }
        else if( node.matches("factor STAR factor") ) {
            if( retType( node.children.get(1) ) == "int*" ) {
                return "int";
            } else {
                bail("can't deference an integer!");
            }
        }
        else if( node.matches("factor AMP factor") ) {
            if( retType( node.children.get(1) ) == "int" && isLValue(node.children.get(1)) ) {
                return "int*";
            } else {
               bail("can't take address of a non-lvalue!");
            }
        }
        else if( node.matches("factor NEW INT LBRACK expr RBRACK") ) {
            if( detValidType(node.children.get(3)) == "int*") {
                bail("can't have an array with size of pointer");
            } else {
                return "int*";
            }
        }
        else {
            return detValidType( node );
        }//else
        return "shouldn't reach this, I think";
    }//retType
    
    // Main program
    public static final void main(String args[]) {
        new WLPPGen().go();
    }

    public void go() {
        Tree parseTree = readParse("S");
        symbol_table = buildTable(parseTree, symbol_table, offset_table);
        checkUses(parseTree, symbol_table);
        checkLValues(parseTree);
        detValidType(parseTree);
        //print_offset_table();
        prologueCode();
        /*storeVars(symbol_table);*/
        exprCode(parseTree);
        epilogueCode();
    }
}
