
import java.util.*;
import java.math.*;

public class RelAsm {
    public static final void main(String[] args) {
        new RelAsm().run();
    }

    private Lexer lexer = new Lexer();

    private void run() {
        Scanner in = new Scanner(System.in);
        
        // an object to store the program
        Program program = new Program();
        
        // make a counter for the number of non-null lines in the program (relocatable so start at 3)
        int lines_in_program = 0;
        
        while(in.hasNextLine()) {
            String line = in.nextLine();

            // Scan the line into an array of tokens
            Token[] temp;
            temp = lexer.scan(line);
            
            // Put the array of tokens into a ProgramLine if non-empty
            if(temp.length != 0){
                
                // put all valid labels in line in the hashtable
                int i;
                for(i = 0; i < temp.length; i++){
                    if(temp[i].kind != Kind.LABEL) { 
                        break; // break if reached a non-label in line
                    } 
                    else {// if label not already in symbol_table, add it to symbol_table
                        String label_only = temp[i].lexeme.substring(0, temp[i].lexeme.length() - 1);
                        if(!program.symbol_table.containsKey(label_only)){ 
                            program.symbol_table.put(label_only, 12+lines_in_program*4); 
                        }
                        else {// if label alredy in symbol table, error
                            System.err.println("ERROR: " + label_only + " already exists.");
                            System.exit(1);
                        }
                    }
                }
                // create array of tokens for non-null lines
                Token[] tokens = new Token[temp.length - i];
                
                boolean instructions_in_line = false;
                
                // if there are instructions in the line; copy them into array tokens    
                for(int j = 0; j < tokens.length; j++){
                    instructions_in_line = true;
                    tokens[j] = temp[i];
                    i++;
                }

                if(instructions_in_line == true){ // if entered loop above (if instructions in line) 
                    
                    // create programline to put tokens in and 
                    ProgramLine program_line = new ProgramLine(tokens);
                    
                    // check if the syntax is okay            
                    program_line.checkSyntax();
                    
                    // add .word lables to reloc_table
                    if(tokens[0].kind == Kind.DOTWORD){
                        if(tokens[1].kind == Kind.ID){
                            program.reloc_table.add(12+(lines_in_program*4));
                        }
                    }
                    
                    // add the line to the program
			        program.addLine(program_line);
			        
			        // increment counter for non-null lines in program
			        lines_in_program += 1;
		        }
            }
        }
        
      int merlcookie =  (4<<26) | (0<<21) | (0<<16) | (2 & 0xffff);
      int endmodule = 12+4*lines_in_program + 8*program.reloc_table.size();
      int endcode = 12+4*lines_in_program;
      
      program.printbytes(merlcookie);
      program.printbytes(endmodule);
      program.printbytes(endcode);
        
		for(int i = 0; i < program.lines.size(); i++){
            
            int toprint=0;
            
            if(program.lines.get(i).tokens[0].kind == Kind.DOTWORD){ // if .word ...
			
				if(program.lines.get(i).tokens[1].kind != Kind.ID){	// if .word number
			        toprint = program.lines.get(i).tokens[1].toInt();
				}
				else {// if .word label
				    String key = program.lines.get(i).tokens[1].lexeme;
				    if(program.symbol_table.containsKey(key)){
				        toprint = program.symbol_table.get(key);
		    	    }
			    	else{
			        	System.err.println("ERROR: undefined reference to label " + key + ".");
               	     	System.exit(1);
			   		}
				}
			}  
			else if(program.lines.get(i).tokens[0].kind == Kind.ID){ //if opcode
			    String opcode = program.lines.get(i).tokens[0].lexeme;
			    
			    if(opcode.equals("jr") | opcode.equals("jalr")){// print appropriate for jr or jalr
			        int s = program.lines.get(i).tokens[1].toInt();
				    if(opcode.equals("jr")){ // if jr
					    toprint = (s << 21) | (8 & 0xffff);
				    }
				    else if(opcode.equals("jalr")){ // if jalr
				        toprint = (s << 21) | (9 & 0xffff);
                    }
                }
                else if(opcode.equals("add") | opcode.equals("sub") | opcode.equals("slt") | opcode.equals("sltu")){// print appropriate for add, sub, or slt
                    int d = program.lines.get(i).tokens[1].toInt();
                    int s = program.lines.get(i).tokens[3].toInt();
                    int t = program.lines.get(i).tokens[5].toInt();
                    if(opcode.equals("add")) {
                        toprint = (s<<21) | (t<<16) | (d<<11) | (32 & 0xffff);
                    }
                    else if(opcode.equals("sub")){
                        toprint = (s<<21) | (t<<16) | (d<<11) | (34 & 0xffff);
                    }
                    else if(opcode.equals("slt")){
                        toprint = (s<<21) | (t<<16) | (d<<11) | (42 & 0xffff);
                    }
                    else if(opcode.equals("sltu")){
                        toprint = (s<<21) | (t<<16) | (d<<11) | (43 & 0xffff);
                    }
                }
                else if(opcode.equals("beq") | opcode.equals("bne")){// print appropriate for beq or bne
                    int s = program.lines.get(i).tokens[1].toInt();
                    int t = program.lines.get(i).tokens[3].toInt();
                    int n = 0;
                    Kind nkind = program.lines.get(i).tokens[5].kind;
                    
                    if( (nkind == Kind.INT & ( n >= Math.pow(2,15) | n < -1*Math.pow(2,15))) |  ((nkind == Kind.HEXINT) & ( n >= Math.pow(2,16) | (n < 0)) ) ){// if out of bounds
                        System.err.println("ERROR: branch offset out of range.");
                        System.exit(1);
                    }
                    
                    if(nkind == Kind.INT | nkind == Kind.HEXINT){// if beq/bne r1 r2 number
                        n = program.lines.get(i).tokens[5].toInt();
                    }
                    else if(nkind == Kind.ID){// if beq/bne r1 r2 label
                        String key = program.lines.get(i).tokens[5].lexeme;
			            if(program.symbol_table.containsKey(key)){
			               int address = program.symbol_table.get(key);
			               n = (address - ((i*4) + 4) -12)/4;
	        	        }
               	        else{
		                   	System.err.println("ERROR: undefined reference to label " + key + ".");
                       	    System.exit(1);
		                }
                    }
                    
                    if(opcode.equals("beq")){
                        toprint = (4<<26) | (s<<21) | (t<<16) | (n & 0xffff);
                    }
                    else if(opcode.equals("bne")){
                        toprint = (5<<26) | (s<<21) | (t<<16) | (n & 0xffff);
                    }
                }
                else if(opcode.equals("lis") | opcode.equals("mflo") | opcode.equals("mfhi")){// print appropriate lis, mflo or mfhi
                    int d = program.lines.get(i).tokens[1].toInt();
                    if(opcode.equals("lis")){
                        toprint = (d<<11) | (20 & 0xffff);
                    }
                    if(opcode.equals("mflo")){
                        toprint = (d<<11) | (18 & 0xffff);
                    }
                    if(opcode.equals("mfhi")){
                        toprint = (d<<11) | (16 & 0xffff);
                    }
                }
                else if(opcode.equals("mult") | opcode.equals("multu") | opcode.equals("div") | opcode.equals("divu")){// print appropriate mult(u) or div(u)
                    int s = program.lines.get(i).tokens[1].toInt();
                    int t = program.lines.get(i).tokens[3].toInt();
                    if(opcode.equals("mult")){
                        toprint = (s<<21) | (t<<16) | (24 & 0xffff);
                    }
                    if(opcode.equals("multu")){
                        toprint = (s<<21) | (t<<16) | (25 & 0xffff);
                    }
                    if(opcode.equals("div")){
                        toprint = (s<<21) | (t<<16) | (26 & 0xffff);
                    }
                    if(opcode.equals("divu")){
                        toprint = (s<<21) | (t<<16) | (27 & 0xffff);
                    }
                }
                else if(opcode.equals("lw") | opcode.equals("sw")){// print appropriate lw or sw
                    int t = program.lines.get(i).tokens[1].toInt();
                    int n = program.lines.get(i).tokens[3].toInt();
                    int s = program.lines.get(i).tokens[5].toInt();
                    Kind nkind = program.lines.get(i).tokens[3].kind;
                    if( (nkind == Kind.INT & ( n >= Math.pow(2,15) | n < -1*Math.pow(2,15))) |  ((nkind == Kind.HEXINT) & ( n >= Math.pow(2,16) | (n < 0)) ) ){
                        System.err.println("ERROR: branch offset out of range.");
                        System.exit(1);
                    }
                    if(opcode.equals("lw")){
                        toprint = (35<< 26) | (s<<21) | (t<<16) | (n & 0xffff);
                    }
                    if(opcode.equals("sw")){
                        toprint = (43<< 26) | (s<<21) | (t<<16) | (n & 0xffff);
                    }
                }
                else{// else error
                    System.err.println("ERROR: " + opcode + " is not an instruction or a label.");
                	 System.exit(1);
                }
			}
			program.printbytes(toprint);
		}
        program.print_symbol_table();
        program.print_reloc_table();
        System.out.flush();
    }
}

// a program
class Program {
    public ArrayList<ProgramLine> lines;
    public HashMap<String, Integer> symbol_table;
    public ArrayList<Integer> reloc_table;

    public Program() {
        lines = new ArrayList<ProgramLine>();
        symbol_table = new HashMap<String, Integer>();
        reloc_table = new ArrayList<Integer>();
    }
    
    // adds a symbol to the symbol table
    public void addSymbol(Token symbol, int address) {
        symbol_table.put(symbol.lexeme, address);
    }
    
    // gets the address of a symbol in the program
    public int getAddress(Token symbol) {
        return Integer.parseInt(symbol_table.get(symbol.lexeme).toString());
    }
    
    // adds a line to the program
    public void addLine(ProgramLine line) {
        this.lines.add(line);
    }
    
    public void printbytes(int toprintvar){
          System.out.write((toprintvar >> 24) & 0xff);
		    System.out.write((toprintvar >> 16) & 0xff);
		    System.out.write((toprintvar >> 8) & 0xff);
		    System.out.write(toprintvar & 0xff);
		    System.out.flush();
    }
    
    // prints the symbol table
    public void print_symbol_table(){
        for (String key : symbol_table.keySet()){ // for each key in keyset of symbol_table
            System.err.println( key + " " + symbol_table.get(key));
        }
    }
    
    public void print_reloc_table(){
        for (int i=0; i< reloc_table.size(); i++){ // for each key in keyset of reloc_table
            System.out.write((1 >> 24) & 0xff);
			   System.out.write((1 >> 16) & 0xff);
			   System.out.write((1 >> 8) & 0xff);
			   System.out.write(1 & 0xff);
            System.out.write((reloc_table.get(i) >> 24) & 0xff);
			   System.out.write((reloc_table.get(i) >> 16) & 0xff);
			   System.out.write((reloc_table.get(i) >> 8) & 0xff);
			   System.out.write(reloc_table.get(i) & 0xff);
            
            System.out.flush();
        }
    }
}

// a line of tokens in the program
class ProgramLine {
    public Token[] tokens;
    
    // constructor
    public ProgramLine(Token[] tokens) {
        this.tokens = tokens;
    }
    
    // returns all the labels inside this line
    public Token[] labels() {
        
        // an array of all the labels
        ArrayList<Token> labels = new ArrayList<Token>();
        
        // add every label on this line to the array
        for(int i = 0; i < tokens.length; i++) {
            if(tokens[i].kind == Kind.LABEL) {
                labels.add(tokens[i]);
            }
        }
        
        // make a copy of the array to return
        Token[] return_array = new Token[labels.size()];
        for(int i = 0; i < return_array.length; i++) {
            return_array[i] = labels.get(i);
        }
        
        // return the array
        return return_array;
    }
    
    public void checkSyntax() {
        if(tokens[0].kind == Kind.DOTWORD) {
            Kind valid_patterns[][] = {
                {Kind.DOTWORD, Kind.HEXINT},
                {Kind.DOTWORD, Kind.INT},
                {Kind.DOTWORD, Kind.ID},
            };
            if(!matchesAPattern(tokens, valid_patterns)) {
                System.err.println("ERROR: Invalid syntax.");
                System.exit(1);
            }
        }
        else if(tokens[0].kind == Kind.ID){
            if(tokens[0].lexeme.equals("jr") || tokens[0].lexeme.equals("jalr")){
                Kind valid_patterns[][] = {
                    {Kind.ID, Kind.REGISTER}
                };
                if(!matchesAPattern(tokens, valid_patterns)) {
                    System.err.println("ERROR: Invalid syntax.");
                    System.exit(1);
                }
            }
            if(tokens[0].lexeme.equals("add") || tokens[0].lexeme.equals("sub") | tokens[0].lexeme.equals("slt") || tokens[0].lexeme.equals("sltu")){
                Kind valid_patterns[][] = {
                    {Kind.ID, Kind.REGISTER, Kind.COMMA, Kind.REGISTER, Kind.COMMA, Kind.REGISTER}
                };
                if(!matchesAPattern(tokens, valid_patterns)) {
                    System.err.println("ERROR: Invalid syntax.");
                    System.exit(1);
                }
            }
            if(tokens[0].lexeme.equals("beq") || tokens[0].lexeme.equals("bne")){
                Kind valid_patterns[][] = {
                    {Kind.ID, Kind.REGISTER, Kind.COMMA, Kind.REGISTER, Kind.COMMA, Kind.HEXINT},
                    {Kind.ID, Kind.REGISTER, Kind.COMMA, Kind.REGISTER, Kind.COMMA, Kind.INT},
                    {Kind.ID, Kind.REGISTER, Kind.COMMA, Kind.REGISTER, Kind.COMMA, Kind.ID},
                };
                if(!matchesAPattern(tokens, valid_patterns)) {
                    System.err.println("ERROR: Invalid syntax.");
                    System.exit(1);
                }
            }
            if(tokens[0].lexeme.equals("lis") || tokens[0].lexeme.equals("mflo") | tokens[0].lexeme.equals("mfhi")){
                Kind valid_patterns[][] = {
                    {Kind.ID, Kind.REGISTER}
                };
                if(!matchesAPattern(tokens, valid_patterns)) {
                    System.err.println("ERROR: Invalid syntax.");
                    System.exit(1);
                }
            }
            if(tokens[0].lexeme.equals("mult") || tokens[0].lexeme.equals("multu") | tokens[0].lexeme.equals("div") || tokens[0].lexeme.equals("divu")){
                Kind valid_patterns[][] = {
                    {Kind.ID, Kind.REGISTER, Kind.COMMA, Kind.REGISTER}
                };
                if(!matchesAPattern(tokens, valid_patterns)) {
                    System.err.println("ERROR: Invalid syntax.");
                    System.exit(1);
                }
            }
            if(tokens[0].lexeme.equals("lw") | tokens[0].lexeme.equals("sw")){
                Kind valid_patterns[][] = {
                    {Kind.ID, Kind.REGISTER, Kind.COMMA, Kind.INT, Kind.LPAREN, Kind.REGISTER, Kind.RPAREN},
                    {Kind.ID, Kind.REGISTER, Kind.COMMA, Kind.HEXINT, Kind.LPAREN, Kind.REGISTER, Kind.RPAREN},
                };
                if(!matchesAPattern(tokens, valid_patterns)) {
                    System.err.println("ERROR: Invalid syntax.");
                    System.exit(1);
                }
            }
        }
        else {
            System.err.println("ERROR: No valid line starts with " + tokens[0] + ".");
            System.exit(1);
        }
    }
    
    public static boolean matchesAPattern(Token[] line, Kind[][] patterns) {
        
        // go through every pattern
        for(int i = 0; i < patterns.length; i++){
            
            // if the patterns are of the same length...
            if(patterns[i].length == line.length) {
                
                // assume that the pattern matches
                boolean pattern_matched = true;
                
                // compare patterns
                for(int j = 0; j < line.length; j++){
                    if(line[j].kind != patterns[i][j]) {
                        pattern_matched = false;
                        break;
                    }
                }
                
                // if they matched, return true
                if(pattern_matched) {
                    return true;
                }
            }
        }
        return false;
    }
}

/** The various kinds of tokens. */
enum Kind {
    ID,         // Opcode or identifier (use of a label)
    INT,        // Decimal integer
    HEXINT,     // Hexadecimal integer
    REGISTER,   // Register number
    COMMA,      // Comma
    LPAREN,     // (
    RPAREN,     // )
    LABEL,      // Declaration of a label (with a colon)
    DOTWORD,    // .word directive
    WHITESPACE; // Whitespace
}

/** Representation of a token. */
class Token {
    public Kind kind;     // The kind of token.
    public String lexeme; // String representation of the actual token in the
                          // source code.

    public Token(Kind kind, String lexeme) {
        this.kind = kind;
        this.lexeme = lexeme;
    }
    public String toString() {
        return kind+" {"+lexeme+"}";
    }
    /** Returns an integer representation of the token. For tokens of kind
     * INT (decimal integer constant) and HEXINT (hexadecimal integer
     * constant), returns the integer constant. For tokens of kind
     * REGISTER, returns the register number.
     */
    public int toInt() {
        if(kind == Kind.INT) return parseLiteral(lexeme, 10, 32);
        else if(kind == Kind.HEXINT) return parseLiteral(lexeme.substring(2), 16, 32);
        else if(kind == Kind.REGISTER) return parseLiteral(lexeme.substring(1), 10, 5);
        else {
            System.err.println("ERROR in to-int conversion.");
            System.exit(1);
            return 0;
        }
    }
    private int parseLiteral(String s, int base, int bits) {
        BigInteger x = new BigInteger(s, base);
        if(x.signum() > 0) {
            if(x.bitLength() > bits) {
                System.err.println("ERROR in parsing: constant out of range: "+s);
                System.exit(1);
            }
        } else if(x.signum() < 0) {
            if(x.negate().bitLength() > bits-1
            && x.negate().subtract(new BigInteger("1")).bitLength() > bits-1) {
                System.err.println("ERROR in parsing: constant out of range: "+s);
                System.exit(1);
            }
        }
        return (int) (x.longValue() & ((1L << bits) - 1));
    }
}

/** Lexer -- reads an input line, and partitions it into a list of tokens. */
class Lexer {
    public Lexer() {
        CharSet whitespace = new Chars("\t\n\r ");
        CharSet letters = new Chars(
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz");
        CharSet lettersDigits = new Chars(
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");
        CharSet digits = new Chars("0123456789");
        CharSet hexDigits = new Chars("0123456789ABCDEFabcdef");
        CharSet oneToNine = new Chars("123456789");
        CharSet all = new AllChars(); 

        table = new Transition[] {
                new Transition(State.START, whitespace, State.WHITESPACE),
                new Transition(State.START, letters, State.ID),
                new Transition(State.ID, lettersDigits, State.ID),
                new Transition(State.START, oneToNine, State.INT),
                new Transition(State.INT, digits, State.INT),
                new Transition(State.START, new Chars("-"), State.MINUS),
                new Transition(State.MINUS, digits, State.INT),
                new Transition(State.START, new Chars(","), State.COMMA),
                new Transition(State.START, new Chars("("), State.LPAREN),
                new Transition(State.START, new Chars(")"), State.RPAREN),
                new Transition(State.START, new Chars("$"), State.DOLLAR),
                new Transition(State.DOLLAR, digits, State.REGISTER),
                new Transition(State.REGISTER, digits, State.REGISTER),
                new Transition(State.START, new Chars("0"), State.ZERO),
                new Transition(State.ZERO, new Chars("x"), State.ZEROX),
                new Transition(State.ZERO, digits, State.INT),
                new Transition(State.ZEROX, hexDigits, State.HEXINT),
                new Transition(State.HEXINT, hexDigits, State.HEXINT),
                new Transition(State.ID, new Chars(":"), State.LABEL),
                new Transition(State.START, new Chars(";"), State.COMMENT),
                new Transition(State.START, new Chars("."), State.DOT),
                new Transition(State.DOT, new Chars("w"), State.DOTW),
                new Transition(State.DOTW, new Chars("o"), State.DOTWO),
                new Transition(State.DOTWO, new Chars("r"), State.DOTWOR),
                new Transition(State.DOTWOR, new Chars("d"), State.DOTWORD),
                new Transition(State.COMMENT, all, State.COMMENT)
        };
    }
    /** Partitions the line passed in as input into an array of tokens.
     * The array of tokens is returned.
     */
    public Token[] scan( String input ) {
        List<Token> ret = new ArrayList<Token>();
        if(input.length() == 0) return new Token[0];
        int i = 0;
        int startIndex = 0;
        State state = State.START;
        while(true) {
            Transition t = null;
            if(i < input.length()) t = findTransition(state, input.charAt(i));
            if(t == null) {
                // no more transitions possible
                if(!state.isFinal()) {
                    System.err.println("ERROR in lexing after reading "+input.substring(0, i));
                    System.exit(1);
                }
                if( state.kind != Kind.WHITESPACE ) {
                    ret.add(new Token(state.kind,
                                input.substring(startIndex, i)));
                }
                startIndex = i;
                state = State.START;
                if(i >= input.length()) break;
            } else {
                state = t.toState;
                i++;
            }
        }
        return ret.toArray(new Token[ret.size()]);
    }

    ///////////////////////////////////////////////////////////////
    // END OF PUBLIC METHODS
    ///////////////////////////////////////////////////////////////

    private Transition findTransition(State state, char c) {
        for( int j = 0; j < table.length; j++ ) {
            Transition t = table[j];
            if(t.fromState == state && t.chars.contains(c)) {
                return t;
            }
        }
        return null;
    }

    private static enum State {
        START(null),
        DOLLAR(null),
        MINUS(null),
        REGISTER(Kind.REGISTER),
        INT(Kind.INT),
        ID(Kind.ID),
        LABEL(Kind.LABEL),
        COMMA(Kind.COMMA),
        LPAREN(Kind.LPAREN),
        RPAREN(Kind.RPAREN),
        ZERO(Kind.INT),
        ZEROX(null),
        HEXINT(Kind.HEXINT),
        COMMENT(Kind.WHITESPACE),
        DOT(null),
        DOTW(null),
        DOTWO(null),
        DOTWOR(null),
        DOTWORD(Kind.DOTWORD),
        WHITESPACE(Kind.WHITESPACE);
        State(Kind kind) {
            this.kind = kind;
        }
        Kind kind;
        boolean isFinal() {
            return kind != null;
        }
    }

    private interface CharSet {
        public boolean contains(char newC);
    }
    private class Chars implements CharSet {
        private String chars;
        public Chars(String chars) { this.chars = chars; }
        public boolean contains(char newC) {
            return chars.indexOf(newC) >= 0;
        }
    }
    private class AllChars implements CharSet {
        public boolean contains(char newC) {
            return true;
        }
    }

    private class Transition {
        State fromState;
        CharSet chars;
        State toState;
        Transition(State fromState, CharSet chars, State toState) {
            this.fromState = fromState;
            this.chars = chars;
            this.toState = toState;
        }
    }
    private Transition[] table;
}

